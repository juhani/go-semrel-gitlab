#!/usr/bin/env bash

echo "Waiting for gitlab to respond..."
while ! curl -s http://gitlab/api/v4/projects > /dev/null; do
    sleep 5
done
echo "Gitlab is up. Giving it one more minute"
sleep 90
echo "Starting tests..."